// OS Project 4
// Patrick (Ducky) Bouchon, Sophie (Bucky) Johnson, Rayyan (Chucky) Karim, Ana Luisa (Lucky) Lamberto
// 3/28/19

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdint.h>

#define    BUFF_BUCKETS       26623
#define    BUCKETS          2662300
#define    SUB_BUCKETS      941176
#define    streq(s0, s1)   (strcmp((s0), (s1)) == 0)

// Usage function
void usage() {
        printf("Usage:\n\t./threadedRE -level Level to Run -thread Number of Threads fileNames\n");
        exit(1);
}

// Struct for packets
struct PacketHolder {
        char data[2400];     // The actual packet data
        uint32_t hash;       // Hash of the packet contents
};

// Struct for packets
struct SubPacketHolder {
        char data[64];       // The actual packet data
        uint32_t hash;       // Hash of the packet contents
};

// Argument struct for producer
struct ProducerArgs {
        char **files;
        int fileCount;
};

// Argument struct for consumer
struct ConsumerArgs {
        int level;
};

// Struct definitions
struct PacketHolder *buffer[BUFF_BUCKETS] = {0};
struct PacketHolder** packetCache;
struct SubPacketHolder** subPacketCache;

// Condition variables and locks
pthread_cond_t empty, fill;
pthread_mutex_t bufferMutex         = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t redundanceMutex     = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t cacheMutex          = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t packetMutex         = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t useMutex            = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t consumerMutex       = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t producerMutex       = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t totalMutex          = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t cache2Mutex         = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t doneMutex           = PTHREAD_MUTEX_INITIALIZER;

// Positions in buffer
int fill_ptr          = 0;
int use_ptr           = 0;
int evict_ptr         = 0;

// Variables for condition variables
int packetCounter     = 0;
int isDone            = 0;

// Accumulation variables
uint32_t redundance        = 0;
uint32_t total             = 0;
uint32_t totalPackets      = 0;

// This is a Jenkins hash from wikipedia
uint32_t hash(char * key, size_t length) {
        size_t i = 0;
        uint32_t hash = 0;
        while (i != length) {
                hash += key[i++];
                hash += hash << 10;
                hash ^= hash >> 6;
        }
        hash += hash << 3;
        hash ^= hash >> 11;
        hash += hash << 15;
        return hash;
}

// Evict packet from cache
void evict(struct PacketHolder** packetCache) {
        while(!packetCache[evict_ptr]){
                evict_ptr = (evict_ptr + 1) % BUCKETS;
        }
        free(packetCache[evict_ptr]);
        packetCache[evict_ptr] = 0;
}

// Function to put packet holder on buffer
void put(struct PacketHolder* tmp) {
        buffer[fill_ptr] = tmp;
        fill_ptr = (fill_ptr + 1) % BUFF_BUCKETS;
        packetCounter += 1;
}

// Function to process individual file
void processFile(FILE *fp){
        if (fp == NULL)  {
          printf("Error: couldn't open file\n");
          return;
        }

        uint32_t nPacketLength;
        char theData[2400];

        fseek(fp, 24, SEEK_CUR);
        while (!feof(fp)) {
                struct PacketHolder *p = (struct PacketHolder*) calloc (1, sizeof (struct PacketHolder));
                if(!p){
                        printf("Calloc Failed;");
                        exit(EXIT_FAILURE);
                }
                // Skip ts sec field
                fseek(fp, 4, SEEK_CUR);

                // Skip ts usec field
                fseek(fp, 4, SEEK_CUR);

                // Read incl_len field
                fread(&nPacketLength, 4, 1, fp);

                uint32_t endOfFile;
                // Skip orig_len field
                fread(&endOfFile, 4, 1, fp);
                if(feof(fp)){
                        break;
                }

                // Check if packet too small
                if (nPacketLength < 128) {
                        fseek(fp, nPacketLength, SEEK_CUR);
                        continue;
                }
                // If valid packet length
                else if (nPacketLength < 2400) {
                        // Read in packet data 
                        memset(theData, 0, 2400);
                        int retVal = fread(theData, 1, nPacketLength, fp);
                        // If successful
                        if (retVal == nPacketLength) {
                                // compute hash on the data
                                memcpy(p->data, theData, sizeof(theData));
                                p->hash = hash(theData, nPacketLength);

                                // Add packet struct to buffer
                                pthread_mutex_lock(&bufferMutex);
                                while (packetCounter == BUFF_BUCKETS) {
                                        pthread_cond_wait(&empty, &bufferMutex);
                                }
                                put(p);
                                pthread_cond_signal(&fill);
                                pthread_mutex_unlock(&bufferMutex);
                        }
                        // Continue if end of file 
                        else{
                                continue;
                        }
                }
                else {
                        fseek(fp, nPacketLength, SEEK_CUR);
                        continue;
                }
        }
        fclose(fp);
}

// Producer
void *producer(void *arg) {
        struct ProducerArgs* arguments = arg;
        for(int i = 0; i < arguments->fileCount; i++) {
                processFile(fopen(arguments->files[i], "r"));
        }
        pthread_mutex_lock(&doneMutex);
        isDone = 1;
        pthread_mutex_unlock(&doneMutex);
        pthread_cond_broadcast(&fill);
        return 0;
}

// Function to get packet holder from buffer
struct PacketHolder* get(void) {
          struct PacketHolder* tmp = buffer[use_ptr];
          use_ptr = (use_ptr + 1) % BUFF_BUCKETS;
          packetCounter -= 1;
          return tmp;
}

// Function to check if packet is in cache
int isRedundant(struct PacketHolder** packetCache, struct PacketHolder* ph) {
        int isRedundant        = 0;
        int replace            = 1;
        int choice             = 1;
        // Find cache entry to check using hash
        int bucket = ph->hash % BUCKETS;
        pthread_mutex_lock(&cacheMutex);
        struct PacketHolder* tmp = packetCache[bucket];
        pthread_mutex_unlock(&cacheMutex);
        if (tmp) {
                // If compared data matches, return true
                if (!memcmp(ph->data, tmp->data, 2400)) {
                        isRedundant = 1;
                        replace = 0;
                }
                else {
                    choice = rand() % 2;
                }
        }
        // Replace based on policy
        if (replace && choice) {
                pthread_mutex_lock(&cacheMutex);
                // Keep under 64MB
                if ((rand() % 2) && totalPackets > 26623) {
                          evict(packetCache);
                          totalPackets -= 1;
                }
                packetCache[bucket] = ph;
                totalPackets += 1;
                pthread_mutex_unlock(&cacheMutex);
        }
        return isRedundant;
}

int isRedundantSub(struct SubPacketHolder** packetCache, struct SubPacketHolder* ph, int update) {
        int isRedundant         = 0;
        int replace             = 1;
        int choice              = 1;
        // Find cache entry to check using hash
        int bucket = ph->hash % SUB_BUCKETS;
        pthread_mutex_lock(&cacheMutex);
        struct SubPacketHolder* tmp = packetCache[bucket];
        pthread_mutex_unlock(&cacheMutex);
        // If entry in cache, check
        if (tmp) {
                // If compared data matches, return true
                if (!memcmp(ph->data, tmp->data, 64)) {
                        isRedundant = 1;
                        replace = 0;
                }
                else {
                        choice = rand() % 2;
                }
        }
        // Otherwise, update no matter what
        else {
                update = 0;
        }
        // Update cache entries
        if (replace && choice && !update) {
                pthread_mutex_lock(&cacheMutex);
                packetCache[bucket] = ph;
                pthread_mutex_unlock(&cacheMutex);
        }
        return isRedundant;
}

// Consumer function
void *consumer(void *arg) {
        // Get level
        int level = ((struct ConsumerArgs*)arg)->level;

        // As long as all packets have not been processed
        while(1) {
                // Read packet holder off of buffer
                pthread_mutex_lock(&bufferMutex);
                while (packetCounter == 0) {
                        // Check to see if all packets have been put on queue
                        pthread_mutex_lock(&doneMutex);
                        if (isDone == 1) {
                                pthread_mutex_unlock(&doneMutex);
                                pthread_mutex_unlock(&bufferMutex);
                                // Exit thread if done
                                return 0;
                        }
                        pthread_mutex_unlock(&doneMutex);
                        // Wait for producer
                        pthread_cond_wait(&fill, &bufferMutex);
                }
                // Get packet off buffer
                struct PacketHolder* ph = get();
                pthread_cond_signal(&empty);
                pthread_mutex_unlock(&bufferMutex);

                // Set local variables
                int localTotal = 0;
                int localRedundance = 0;

                // Check entire payload if level 1
                if(level == 1) {
                        // Record if redundant
                        localTotal += 1;
                        if (isRedundant(packetCache, ph)) {
                                localRedundance += 1;
                        }
                }

                // Check 64 byte windows if level 2
                else if (level == 2) {
                        char window[64];
                        int i = 52;
                        localTotal += 63;
                        int lastInCache = 64;
                        int counter = 0;
                        while(ph->data[i+64] != '\0') {
                                counter += 1;
                                //mini packets since last update
                                lastInCache += 1;
                                // reset window to 0
                                memset(window, 0, 64);
                                // window becomes the 64 bytes of packet we want to look at
                                memcpy(window,&(ph->data[i]),64);

                                // build the subpacket holder
                                struct SubPacketHolder *sph = (struct SubPacketHolder*) calloc (1, sizeof (struct SubPacketHolder));
                                if(!sph){
                                        printf("Failed to Calloc\n");
                                        exit(EXIT_FAILURE);
                                }
                                memcpy(sph->data,window,64);
                                sph->hash =  hash(sph->data,64);

                                // Track total bytes
                                localTotal += 1;

                                // Record if redundant
                                if (isRedundantSub(subPacketCache, sph, counter % 32)) {
                                        // 64 bytes were redundant
                                        if (lastInCache < 64) {
                                                localRedundance += lastInCache;
                                        }

                                        else {
                                                localRedundance += 64;
                                        }
                                        lastInCache = 0;
                                }
                                i++;
                        }
                }

                // Update global total
                pthread_mutex_lock(&totalMutex);
                total += localTotal;
                pthread_mutex_unlock(&totalMutex);
                // Update global redundance
                pthread_mutex_lock(&redundanceMutex);
                redundance += localRedundance;
                pthread_mutex_unlock(&redundanceMutex);

        }
        return 0;
}

int main(int argc, char* argv[]){
        int thread          = 4;
        int argind          = 1;
        int fileCount       = 0;
        int level           = 2;
        char *files[10];

        //Command Line Ingestion
        while (argind < argc && argv[argind][0] == '-') {
                if(streq(argv[argind], "-help")) {
                        usage();
                }
                else if(streq(argv[argind], "-level")) {
                        level = atoi(argv[++argind]);
                        if (level < 1 || level > 2) {
                          printf("Choose either level 1 or level 2 por favor\n");
                          usage();
                        }
                }
                else if(streq(argv[argind], "-thread")) {
                        thread = atoi(argv[++argind]);
                        if (thread < 2) {
                          printf("You absolute fool -- minimum of two threads\n");
                          usage();
                        }
                }
                else{
                        usage();
                }
                ++argind;
        }

        if (argind == argc) {
                printf("Please enter files\n");
                usage();

        }

        // Record files
        while(argind < argc) {
                files[fileCount++] = argv[argind++];
        }

        // check real swif that files actually can be opened
        for (int i = 0; i < fileCount; i++) {
          FILE *fp = fopen(files[i], "r");
          if (fp == NULL) {
            printf("Unfortunately we could not access one or more files :(\n");
            exit(1);
          } else {
            fclose(fp);
          }
        }


        // Create cache based on level
        if (level == 1) {
                packetCache = calloc(BUCKETS, sizeof(struct PacketHolder*));
                if (!packetCache) {
                        printf("Calloc failed. Unable to create cache.\n");
                        exit(EXIT_FAILURE);
                }
        }
        else if (level == 2) {
                subPacketCache = calloc(SUB_BUCKETS, sizeof(struct SubPacketHolder*));
                if (!subPacketCache) {
                        printf("Calloc failed. Unable to create cache.\n");
                        exit(EXIT_FAILURE);
                }
        }


        //Introduction
        printf("Welcome to Project 4 - ThreadedRE by TeamRockOn\n");
        if(level == 1) {
                printf("Now operating in Level 1 mode - full payload detection Threads Allowed: %d\n", thread);
        }
        else{
                printf("Now operating in Level 2 mode - specified detection Threads Allowed: %d\n", thread);
        }

        printf("Allocating 1 thread to producer and %d threads to consumer to process: ", thread - 1);
        for(int i = 0; i < fileCount; i++) {
                printf("%s ", files[i]);
        }
        printf("\n");

        // Arguments for function to call producer thread
        struct ProducerArgs* pArg = calloc(1, sizeof(struct ProducerArgs));
        if (!pArg) {
          printf("Malloc failed sadly :()\n");
          exit(EXIT_FAILURE);
        }

        pArg->files = files;
        pArg->fileCount = fileCount;

        // Call producer
        pthread_t producer_id;
        pthread_create(&producer_id, NULL, producer, pArg);
        // Make array for consumers and allocate for arguments
        pthread_t consumers[thread - 1];
        struct ConsumerArgs* cArg = calloc(thread - 1, sizeof(struct ConsumerArgs));
        if (!cArg) {
                printf("%s: calloc failed\n", argv[0]);
                exit(EXIT_FAILURE);
        }
        // Create threads
        for (int i = 0; i < thread - 1; i++) {
                // Update thread argument with values
                cArg[i].level = level;
                if (pthread_create(&consumers[i], NULL, consumer, &cArg[i]) != 0) {
                        printf("%s: failed to create thread\n", argv[0]);
                        free(cArg);
                        exit(EXIT_FAILURE);
                }
        }

        // Wait for producer to complete
        pthread_join(producer_id, NULL);
        // Wait for consumers to complete
        for (int i = 0; i < thread - 1; i++) {
                // Join threads, check for error
                if (pthread_join(consumers[i], NULL) != 0) {
                        printf("%s: failed to join thread\n", argv[0]);
                        free(cArg);
                        exit(EXIT_FAILURE);
                }
        }

        // Report results for level 1
        printf("\n");
        if (level == 1) {
                printf("Packets Processed: %d\n", total);
                printf("Hits: %d Packets\n", redundance);
                printf("Redundancy Detected: %.2f%%\n", (redundance / (float) total * 100));

        }
        else if (level == 2) {
                printf("Data Processed: %d Bytes\n", total);
                printf("Hits: %d Bytes\n", redundance);
                printf("Redundancy Detected: %.2f%%\n", (redundance / (float) total * 100));
        
        }

        // Free allocated structs
        free(pArg);
        free(cArg);

        // Free packets in cache for level 1
        if (level == 1) {
                for (int i = 0; i < BUCKETS; i++) {
                        if (packetCache[i]) {
                                free(packetCache[i]);
                        }
                }
        } 
        // Free packets in cache for level 2
        else if (level == 2) {
                for (int i = 0; i < SUB_BUCKETS; i++) {
                        if (subPacketCache[i]) {
                                free(subPacketCache[i]);
                        }
                }
        }
        
        // Free cache
        free(packetCache);

        return 0;
}
